/**
 * Created by heren98 on 20/10/17.
 */
;(function ($) {

    "use strict";

    // Defaults options
    var pluginName = "myTimesTable",
        defaultOptions = {
            "days": ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'],
            "startHour": 8,
            "timeText": "h", // ou am
            "timeIntervalle": "5", // ou 10 ou 15 ou 30 ou 60
            "events": []
        };

    var X = 7, Y = 156, P = 5;

    // Plugin constructor
    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaultOptions, options);
        this._defaults = defaultOptions;
        this._name = pluginName;
        this.repere = {"X": 7, "Y": 156, "P": 5};
        this.init();
    }

    $.extend(Plugin.prototype, {
        init: function () {
            var $this = this;

            $(this.element).addClass('myTimesTable');

            this.generate();
        },

        generate: function () {
            var $table = $('<table/>'),
                $thead = $('<tr class="days"/>');

            $thead.append('<td></td><td></td>');
            console.log(this.settings.days);

            for (var k = 0; k < 7; k++) {
                $thead.append('<td class="col">' + this.settings.days[k] + '</td>');
            }

            $thead.append('<td></td><td></td>');

            $table.append($thead);

            var hour = this.settings.startHour, h = "00";

            for (var i = 0; i < this.repere.Y; i += this.repere.P) {
                if (i !== 0) {
                    $table.append('<tr><td></td><td></td>' +
                        '<td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td>' +
                        '<td></td><td></td></tr>');
                }
                $table.append('<tr><td rowspan="2">' + hour + 'h' + h + '</td><td></td>' +
                    '<td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td>' +
                    '<td></td><td rowspan="2">' + hour + 'h' + h + '</td></tr>');
                $table.append('<tr><td></td>' +
                    '<td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td>' +
                    '<td></td></tr>');

                $table.append('<tr><td></td><td></td>' +
                    '<td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td>' +
                    '<td></td><td></td></tr>');
                $table.append('<tr><td></td><td></td>' +
                    '<td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td>' +
                    '<td></td><td></td></tr>');
                $table.append('<tr><td></td><td></td>' +
                    '<td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td><td class="yt"></td>' +
                    '<td></td><td></td></tr>');

                if (h === '00') {
                    h = '30'
                } else {
                    h = '00';
                    hour++
                }
            }

            $(this.element).append($table);

            for(var v = 0; v < this.settings.events.length; v++) {
                this.placeEvent(this.settings.events[v]);
            }
        },

        convertTimeToPoint: function (x, start, end) {
            var vals1 = start.split('h'), y = 0, z = 0, vals2 = end.split('h');

            if (vals1[0] < 7 || vals1[1] % 5 !== 0 || vals2[0] < 7 || vals2[1] % 5 !== 0) {
                console.log("Error parsing time");
            }

            var startMin = Number(vals1[0])*60+Number(vals1[1]) - 420,
                endMin = Number(vals2[0])*60+Number(vals2[1]) - 420;

            console.log(startMin);
            console.log(endMin);

            y = startMin / 5;
            z = endMin / 5;
            z = (endMin - startMin) / 5;

            return {"x": x, "y": y, "z": z};
        },

        placeEvent: function (event) {
            var point = this.convertTimeToPoint(event.day, event.start, event.end);
            console.log(point);
            var $cell = $('.myTimesTable table tr:eq(' + (point.y + 2) + ') td.yt:eq(' + (point.x) + ')');

            $cell.attr('rowspan', point.z);

            $cell.css({
                'background-color': '#2b542c',
                'color': '#FFF'
            });

            $cell.append('<div class="T">' +
                '<div class="H">'+event.start+' - '+event.end+'</div>' +
                '    <div class="B">' +
                '        <div class="M">' + event.matiere.nom +
                '        </div>' +
                '        <div class="C">' + event.classe.nom +
                '        </div>' +
                '    </div>' +
                '</div>');

            this.removeTableCell(point);
        },

        removeTableCell: function (point) {
            for(var p = point.y+1; p < point.y + point.z; p++) {
                var $cell = $('.myTimesTable table tr:eq(' + (p+2) + ') td.yt:eq(' + (point.x) + ')');
                console.log($cell);
                $cell.hide();
            }
        }
    });

    $.fn[pluginName] = function (options) {

        new Plugin(this, options);

        return this;
    };

})(jQuery);